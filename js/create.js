/*var crypto = require('crypto');*/
const crypto = require('crypto');
crypto.DEFAULT_ENCODING = 'hex';
crypto.pbkdf2('secret', 'salt', 100000, 512, 'sha512', (err, derivedKey) => {
  if (err) throw err;
  console.log(derivedKey);  // '3745e48...aa39b34'
});
var pbkdf2 = require('pbkdf2')
var derivedKey = pbkdf2.pbkdf2Sync('password', 'salt', 1, 32, 'sha512')
let express = require('express');
let app = express();

console.log("Works");

app.use(express.static("static"));

app.get("/", function(req, res){
	res.sendFile('create.html',  {root: __dirname})
});
app.get("/", function(req, res){
	res.sendFile('create.js',  {root: __dirname})
});

app.listen(3000, "localhost", function(err, result) {
	console.log("Your server works 3000 : port")
});

function createAccount() {
	var fn;
	var ln;
	var date = [];
	var un;
	var pw;
	var pw2;
	var hashed_pw;
	var salt = "Nx#cM>25*d]HE&`q";

	pw = $("#password").val();
	pw2 = $("#ConPassword").val();

	// If password is not the same
	if ((pw.localeCompare(pw2) != 0)) {
		console.log("In here");
		$(".passwordsNotEqual").html("PASSWORDS NOT EQUAL");
	}
	// If passwords are the same
	else {
		/*https://www.owasp.org/index.php/Password_Storage_Cheat_Sheet*/
		fn = $("#firstname").val();
		ln = $("#lastname").val();
		date = parseDate($("#DOB").val());
		un = $("#username").val();
		hashed_pw = encryptPassword($("#password").val());
		console.log(date);
		$(".passwordsNotEqual").html("PASSWORDS EQUAL");
		console.log(pbkdf2('password', salt, 1, 32, 'sha512'));
		$(".passwordsNotEqual").html("PASSWORDS Hashed");
		$(".accountCreated").html(hashed_pw);
		console.log(date[0]);
	}
}

function encryptPassword(password) {
	/*https://code.google.com/archive/p/crypto-js/*/
	/*https://github.com/brix/crypto-js*/
}

function accountToXML(fn, ln, date, un, hashed_pw) {
	var len = 12;

}

function parseDate(dateString) {
	// Year, month, day
	var dateList = dateString.split("-");
	return dateList;
}

/*jQuery.ajax({
    type: "GET",
    url: "data/XMLoutline.xml",
    dataType: "text",
    error: function(){
        alert('Error loading XML document');
    },
    success: parseXML()
    });*/

function parseXML() {
	var xmlText;
	var parser;
	var XMLObject;

	/*https://www.w3schools.com/xml/xml_parser.asp*/

	/*$.get("XMLoutline.xml", function(data)) {
		console.log(data);
	}, 'text');*/

}
/*https://stackoverflow.com/questions/14446447/how-to-read-a-local-text-file*/
/*function readTextFile(file)
{
    var rawFile = new XMLHttpRequest();
    rawFile.open("GET", file, false);
    rawFile.onreadystatechange = function ()
    {
        if(rawFile.readyState === 4)
        {
            if(rawFile.status === 200 || rawFile.status == 0)
            {
                var allText = rawFile.responseText;
                alert(allText);
            }
        }
    }
    rawFile.send(null);
}*/
function pbkdf2(password, salt, iterations, len, hashType) {
  hashType = hashType || 'sha1';
  if (!Buffer.isBuffer(password)) {
    password = new Buffer(password);
  }
  if (!Buffer.isBuffer(salt)) {
    salt = new Buffer(salt);
  }
  var out = new Buffer('');
  var md, prev, i, j;
  var num = 0;
  var block = Buffer.concat([salt, new Buffer(4)]);
  while (out.length < len) {
    num++;
    block.writeUInt32BE(num, salt.length);
    prev = crypto.createHmac(hashType, password)
      .update(block)
      .digest();
    md = prev;
    i = 0;
    while (++i < iterations) {
      prev = crypto.createHmac(hashType, password)
        .update(prev)
        .digest();
      j = -1;
      while (++j < prev.length) {
        md[j] ^= prev[j]
      }
    }
    out = Buffer.concat([out, md]);
  }
  return out.slice(0, len);
}