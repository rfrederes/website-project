var crypto = require('crypto')
  , shasum = crypto.createHash('sha1')
  , shasum2 = crypto.createHash('sha1');
//shasum.update("foo");
//shasum2.update("foo");

var myFavoritePie = "Pumpkin";
var nonce = "peach";
var myFavoritePie2 = "Pumpkim";

/// Cryptographic hash
/// Big changes in hash on minor changes in string
/// Irreversible
/// Easy to confirm given the string
console.log(crypto.createHash('sha1').update(myFavoritePie).digest('hex'));
console.log(crypto.createHash('sha1').update(myFavoritePie + nonce).digest('hex'));
console.log(crypto.createHash('sha1').update(myFavoritePie2).digest('hex'));

/*let cont = true;
let count = 0;
while(cont)
{
    let nonce = Math.random();
    let result = crypto.createHash('sha1').update(myFavoritePie + nonce).digest('hex');
    
    if(result.endsWith("0000000"))
    {
        cont = false;
        console.log ("You got it: " + nonce);
    }
    count++;
    if(count % 1000 == 0)
    {
        console.log(count);
    }
}*/